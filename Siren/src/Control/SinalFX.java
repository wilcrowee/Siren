package Control;

import View.MainController;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.quartz.*;

public class SinalFX extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Platform.setImplicitExit(false);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/Main.fxml"));
        Parent root = loader.load();
        MainController controller = loader.getController();
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.getIcons().add(new Image("icones/mermaid.png"));
        stage.setTitle("IFSP Siren");
        stage.show();
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                try {
                    controller.shutdown();
                } catch (SQLException ex) {
                    Logger.getLogger(SinalFX.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SchedulerException ex) {
                    Logger.getLogger(SinalFX.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                Platform.exit();
            }
        });
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
}
