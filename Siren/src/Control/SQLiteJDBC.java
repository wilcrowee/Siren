package Control;

import java.sql.*;

public class SQLiteJDBC {
    
    public Connection SQLiteConnect(){
        Connection con = null;
        Statement stmt = null;
        
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:.IFSP_Siren.db");

            stmt = con.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS Horario (" +
                        "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "nome char(25)," + 
                        "hora char(8)," +
                        "seg INTEGER, ter INTEGER, qua INTEGER, qui INTEGER, sex INTEGER, tipo INTEGER)"; 

            stmt.executeUpdate(sql);
            stmt.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        System.out.println("Opened database successfully");
        return con;
    }
}