package Control;

import Model.Horario;
import Model.Sinal;
import Model.SinalAr;
import Model.SinalAviso;
import Model.SinalGeral;
import View.MainController;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class Agendamento {
    private static Agendamento agendamento;
    private Scheduler scheduler;
    private List<Horario> horarios;

    public static Agendamento getAgendamento(MainController main) throws SQLException{
        if (agendamento == null) {
            agendamento = new Agendamento(main);
            agendamento.populateHorarios(main);
        }
        
        return agendamento;
    }
    
    public void setAgendamento(Agendamento ag, MainController main) throws SQLException{
        Agendamento.agendamento = ag;
        this.populateHorarios(main);
    }
    
    public List<Horario> getHorarios(){
        return horarios;
    }

    public Scheduler getScheduler() {
        return scheduler;
    }
    
    private Agendamento(MainController main) {
        try {
            scheduler = StdSchedulerFactory.getDefaultScheduler();
            horarios = new ArrayList<>();
        } catch (SchedulerException ex) {
            main.setStatus("Desligado");
            main.setStatusColor(-1);
            ex.printStackTrace();
        }
    }
    
    private void populateHorarios(MainController main) throws SQLException {
        SQLiteJDBC db = new SQLiteJDBC();
        Statement stmt;
        ResultSet rs;
        String sql = "SELECT * FROM Horario";
        
        horarios.clear();
        
        try (Connection con = db.SQLiteConnect()) {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                Horario horario = new Horario();
                horario.setID(rs.getInt("ID"));
                horario.setNome(rs.getString("nome"));
                horario.setHora(rs.getString("hora"));
                horario.setTipo(rs.getInt("tipo"));
                
                if(rs.getInt("seg") == 0){
                    horario.setSeg(false);
                }else{
                    horario.setSeg(true);
                }
                
                if(rs.getInt("ter") == 0){
                    horario.setTer(false);
                }else{
                    horario.setTer(true);
                }
                
                if(rs.getInt("qua") == 0){
                    horario.setQua(false);
                }else{
                    horario.setQua(true);
                }
                if(rs.getInt("qui") == 0){
                    horario.setQui(false);
                }else{
                    horario.setQui(true);
                }
                
                if(rs.getInt("sex") == 0){
                    horario.setSex(false);
                }else{
                    horario.setSex(true);
                }
 
                horarios.add(horario);
            }
            
            stmt.close();
            con.close();
        }
    }
    
    private void horario(String nameJob, String nameTrigger, String cronExpression) throws SchedulerException {
        CronTrigger trigger = newTrigger().withIdentity(nameTrigger, "groupSinal").withSchedule(cronSchedule(cronExpression)).build();
        JobDetail job = newJob(Sinal.class).withIdentity(nameJob, "groupSinal").build();
        scheduler.scheduleJob(job, trigger);
    }
    
    private void horarioAviso(String nameJob, String nameTrigger, String cronExpression) throws SchedulerException {
        CronTrigger trigger = newTrigger().withIdentity(nameTrigger, "groupSinal").withSchedule(cronSchedule(cronExpression)).build();
        JobDetail job = newJob(SinalAviso.class).withIdentity(nameJob, "groupSinal").build();
        scheduler.scheduleJob(job, trigger);
    }

    private void horarioAr(String nameJob, String nameTrigger, String cronExpression) throws SchedulerException {
        CronTrigger trigger = newTrigger().withIdentity(nameTrigger, "groupSinal").withSchedule(cronSchedule(cronExpression)).build();
        JobDetail job = newJob(SinalAr.class).withIdentity(nameJob, "groupSinal").build();
        scheduler.scheduleJob(job, trigger);
    }
    
    private void horarioGeral(String nameJob, String nameTrigger, String cronExpression) throws SchedulerException {
        CronTrigger trigger = newTrigger().withIdentity(nameTrigger, "groupSinal").withSchedule(cronSchedule(cronExpression)).build();
        JobDetail job = newJob(SinalGeral.class).withIdentity(nameJob, "groupSinal").build();
        scheduler.scheduleJob(job, trigger);
    }
    
    public boolean scheduleAgendamento(MainController main){
        try {
            int count = 0;
            scheduler.clear();
            
            for(Horario item:horarios){
                String[] hora, dias;
                String cron = "";
                int aux = 0;
                
                hora = item.getHora().split(":");
                cron = hora[2]+" "+hora[1]+" "+hora[0]+" ? * ";
                
                if(item.isSeg()){
                    cron = cron+"MON";
                    aux++;
                }
                if(item.isTer()){
                    if(aux != 0){
                        cron = cron+",";
                    }
                    cron = cron+"TUE";
                    aux++;
                }
                if(item.isQua()){
                    if(aux != 0){
                        cron = cron+",";
                    }
                    cron = cron+"WED";
                    aux++;
                }
                if(item.isQui()){
                    if(aux != 0){
                        cron = cron+",";
                    }
                    cron = cron+"THU";
                    aux++;
                }
                if(item.isSex()){
                    if(aux != 0){
                        cron = cron+",";
                    }
                    cron = cron+"FRI";
                    aux++;
                }
                
                switch(item.getTipo()){
                    case 0:
                        agendamento.horario("Job"+count, item.getNome(), cron);
                        count++;
                        break;
                    case 1:
                        agendamento.horarioAviso("Job"+count, item.getNome(), cron);
                        count++;
                        break;
                    case 2:
                        agendamento.horarioAviso("Job"+count, item.getNome(), cron);
                        count++;
                        break;
                    case 3:
                        agendamento.horarioGeral("Job"+count, item.getNome(), cron);
                        count++;
                        break;
                    default:
                        agendamento.horario("Job"+count, item.getNome(), cron);
                        count++;
                        break;
                }
            }

            scheduler.start();
            main.setStatus("Ligado");
            main.setStatusColor(0);
            return true;
            
        } catch (SchedulerException ex) {
            main.setStatus("Desligado");
            main.setStatusColor(1);
            ex.printStackTrace();
            return false;
        }
    }
    
    public void stopSchedule() throws SchedulerException{
        scheduler.shutdown(true);
    }
    
    public void cleanSchedule() throws SchedulerException{
        scheduler.clear();
    }
}