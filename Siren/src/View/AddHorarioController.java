package View;

import Control.Agendamento;
import Control.MaskCampos;
import Control.SQLiteJDBC;
import Model.Horario;
import Model.Tipo;
import java.net.URL;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import org.quartz.SchedulerException;

public class AddHorarioController implements Initializable {
    
    private Horario horario;
    @FXML
    private TextField tfNome;
    @FXML
    private TextField tfHorario;
    @FXML
    private CheckBox cbSeg;
    @FXML
    private CheckBox cbTer;
    @FXML
    private CheckBox cbQua;
    @FXML
    private CheckBox cbQui;
    @FXML
    private CheckBox cbSex;
    @FXML
    private TextField tfID;
    @FXML
    private Button bExcluir;
    @FXML
    private Button bSalvar;
    @FXML
    private Button bCancelar;
    @FXML
    private ComboBox<Tipo> cbTipo;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        List<Tipo> lista = new ArrayList<>();
        lista.add(new Tipo("Sinal",0));
        lista.add(new Tipo("Alerta",1));
        lista.add(new Tipo("Ar",2));
        lista.add(new Tipo("Fechamento",3));
               
        tfID.setDisable(true);
        cbTipo.getItems().addAll(lista);
                
        MaskCampos.timeField(tfHorario);
        MaskCampos.maxField(tfNome, 30);
    }    

    @FXML
    private void clickBotaoExcluir(ActionEvent event) {
        SQLiteJDBC db = new SQLiteJDBC();
        PreparedStatement pstmt;
        List<Horario> horarios;        
        
        if(horario == null || horario.getID() == 0){
            warningDialog("Erro ao carregar o registro para exclusão", "Aviso");
        }else{
            try (Connection con = db.SQLiteConnect()) {
                pstmt = con.prepareStatement("DELETE FROM Horario WHERE ID = ?");
                pstmt.setInt(1, horario.getID());
                pstmt.execute();
                pstmt.close();
                con.close();
                
                confirmDialog("Registro Excluido com Sucesso", "Sucesso");
                ((Stage)(bSalvar.getScene().getWindow())).close();
            }catch(SQLException ex){
                errorDialog("Erro ao deletar o registro do banco de dados", "Erro");
            }
            
        }       
    }

    @FXML
    private void clickBotaoSalvar(ActionEvent event) throws SQLException, SchedulerException {
        SQLiteJDBC db = new SQLiteJDBC();
        PreparedStatement pstmt;
        List<Horario> horarios;        
        
        if(horario == null){
            warningDialog("Erro ao carregar o registro para edição", "Aviso");
        }else{
            if(validaCampos()){
                try (Connection con = db.SQLiteConnect()) {
                if(horario.getID() == 0){
                    pstmt = con.prepareStatement("INSERT INTO Horario(nome, hora, seg, ter, qua, qui, sex, tipo) VALUES (?,?,?,?,?,?,?,?)");
                }else{
                    pstmt = con.prepareStatement("UPDATE Horario SET nome = ?, hora = ?, seg = ?, ter = ?, qua = ?, qui = ?, sex = ?, tipo = ? WHERE ID = ?");
                    pstmt.setInt(9, horario.getID());
                }
                
                pstmt.setString(1, tfNome.getText());
                pstmt.setString(2, tfHorario.getText());
                
                if(cbSeg.isSelected()){
                    pstmt.setInt(3, 1);
                }else{
                    pstmt.setInt(3, 0);
                }
                if(cbTer.isSelected()){
                    pstmt.setInt(4, 1);
                }else{
                    pstmt.setInt(4, 0);
                }
                if(cbQua.isSelected()){
                    pstmt.setInt(5, 1);
                }else{
                    pstmt.setInt(5, 0);
                }
                if(cbQui.isSelected()){
                    pstmt.setInt(6, 1);
                }else{
                    pstmt.setInt(6, 0);
                }
                if(cbSex.isSelected()){
                    pstmt.setInt(7, 1);
                }else{
                    pstmt.setInt(7, 0);
                }
                
                pstmt.setInt(8, cbTipo.getSelectionModel().getSelectedItem().getId());
                
                pstmt.execute();
                pstmt.close();
                con.close();
                
                confirmDialog("Registro Salvo com Sucesso", "Sucesso");
                ((Stage)(bSalvar.getScene().getWindow())).close();
                }catch(SQLException ex){
                    errorDialog("Erro ao salvar o registro no banco de dados", "Erro");
                }
            }
        }       
    }

    @FXML
    private void clickBotaoCancelar(ActionEvent event) {
        ((Stage)(bCancelar.getScene().getWindow())).close();
    }    
    
    private void confirmDialog(String msg, String title){
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(msg);
        alert.showAndWait();
    }
    
    private void warningDialog(String msg, String title){
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(msg);
        alert.showAndWait();
    }
    
    private void errorDialog(String msg, String title){
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(msg);
        alert.showAndWait();
    }
    
    private boolean validaCampos() throws SQLException, SchedulerException{
        List<Horario> lista;
        String[] partes;
        String nome = tfNome.getText();
        String hora = tfHorario.getText();
        
        if(nome.length() < 2){
            errorDialog("O campo nome deve ter mais de dois caracteres.", "Erro");
            return false;
        }
        if(hora.length() != 8){
            errorDialog("O campo horario deve ter estar no padrão 00:00:00.", "Erro");
            return false;
        }
        if(!cbSeg.isSelected() && !cbTer.isSelected() && !cbQua.isSelected() && !cbQui.isSelected() && !cbSex.isSelected()){
            errorDialog("Ao menos um dia deve ser selelcionado.", "Erro");
            return false;
        }
               
        partes = tfHorario.getText().split(":");
        
        if(Integer.parseInt(partes[0]) >= 24){
            errorDialog("Hora inválida: Insira um valor de 00 as 23.", "Erro");
            return false;
        }
        if(Integer.parseInt(partes[1]) >= 60){
            errorDialog("Minutos inválidos: Insira um valor de 00 as 59.", "Erro");
            return false;
        }
        if(Integer.parseInt(partes[2]) >= 60){
            errorDialog("Segundos inválidos: Insira um valor de 00 as 59.", "Erro");
            return false;
        }
        
        lista = Agendamento.getAgendamento(new MainController()).getHorarios();
        
        if(lista.isEmpty()){
            return true;
        }else{
            for(Horario item:lista){
                if(item.getHora().equals(tfHorario.getText()) &&  item.getID()!=Integer.parseInt(tfID.getText())){
                    if(cbSeg.isSelected() && item.isSeg()){
                        errorDialog("O horario desejado conflita com outro previamente cadastrado.", "Erro");
                        return false;
                    }
                    if(cbTer.isSelected() && item.isTer()){
                        errorDialog("O horario desejado conflita com outro previamente cadastrado.", "Erro");
                        return false;
                    }
                    if(cbQua.isSelected() && item.isQua()){
                        errorDialog("O horario desejado conflita com outro previamente cadastrado.", "Erro");
                        return false;
                    }
                    if(cbQui.isSelected() && item.isQui()){
                        errorDialog("O horario desejado conflita com outro previamente cadastrado.", "Erro");
                        return false;
                    }
                    if(cbSex.isSelected() && item.isSex()){
                        errorDialog("O horario desejado conflita com outro previamente cadastrado.", "Erro");
                        return false;
                    }
                }
            }
        }
        
        return true;
    }
    
    public void setCampos(Horario horario){
        this.horario = horario;
        tfID.setText(""+horario.getID());
        tfNome.setText(horario.getNome());
        tfHorario.setText(horario.getHora());
        cbSeg.setSelected(horario.isSeg());
        cbTer.setSelected(horario.isTer());
        cbQua.setSelected(horario.isQua());
        cbQui.setSelected(horario.isQui());
        cbSex.setSelected(horario.isSex());
        
        int index = 0;
        for(Tipo item : cbTipo.getItems()){
            if(item.getId() == horario.getTipo()){
                cbTipo.getSelectionModel().select(index);
                break;
            }
            index++;
        }
    }   
}
