package View;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import Control.Agendamento;
import Control.SQLiteJDBC;
import Model.Horario;
import Model.Sinal;
import Model.SinalAr;
import Model.SinalAviso;
import Model.SinalGeral;
import Model.bean_horario;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.UnsupportedCommOperationException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import org.quartz.SchedulerException;

public class MainController implements Initializable {

    @FXML
    private Label labelStatus;
    @FXML
    private Button bSinal;
    @FXML
    private Button bAviso;
    @FXML
    private Button bAr;
    @FXML
    private Button bIniciar;
    @FXML
    private Button bNovo;
    @FXML
    private Button bEditar;
    @FXML
    private Button bDeletar;
    @FXML
    private TableView<Horario> tvHorario;
    @FXML
    private Button bAr1;
    @FXML
    private ImageView ivStartButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        List<Horario> horarios;
        
        try {
            Agendamento ag = Agendamento.getAgendamento(this);
            horarios = ag.getHorarios();
            preencheTabela(horarios);
            ag = null;
        } catch (SQLException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }    

    @FXML
    private void clickBotaoIniciar(ActionEvent event) throws SQLException, SchedulerException, URISyntaxException {
        Agendamento ag = Agendamento.getAgendamento(this);
        
        if(bIniciar.getText().equals("Iniciar")){
            List<Horario> horarios = ag.getHorarios();
            preencheTabela(horarios);
            
            if(ag.scheduleAgendamento(this)){
                Image image = new Image(getClass().getResource("/icones/stop.png").toURI().toString());
                ivStartButton.setImage(image);
                bIniciar.setText("Parar");
                
            }else{
                ag.cleanSchedule();
                Image image = new Image(getClass().getResource("/icones/play.png").toURI().toString());
                ivStartButton.setImage(image);
                bIniciar.setText("Iniciar");
                setStatus("Desligado");
                setStatusColor(1);
                errorDialog("Erro ao criar os alarmes", "Erro");
            }
        }else{
            ag.cleanSchedule();
            Image image = new Image(getClass().getResource("/icones/play.png").toURI().toString());
            ivStartButton.setImage(image);
            bIniciar.setText("Iniciar");
            setStatus("Desligado");
            setStatusColor(1);
        }                
    }
    
    @FXML
    private void clickBotaoSinal(ActionEvent event) {
        try {
            Sinal sinal = new Sinal();
            sinal.tocar();
        } catch (NoSuchPortException ex) {
            ex.printStackTrace();
        } catch (PortInUseException ex) {
            ex.printStackTrace();
        } catch (UnsupportedCommOperationException ex) {
            ex.printStackTrace();
        }  
    }

    @FXML
    private void clickBotaoAviso(ActionEvent event) {
        try {
            SinalAviso sinal = new SinalAviso();
            sinal.tocar();
        } catch (NoSuchPortException ex) {
            ex.printStackTrace();
            errorDialog(ex.getMessage(), "ERRO");
        } catch (PortInUseException ex) {
            ex.printStackTrace();
            errorDialog(ex.getMessage(), "ERRO");
        } catch (UnsupportedCommOperationException ex) {
            errorDialog(ex.getMessage(), "ERRO");
        }  
    }

    @FXML
    private void clickBotaoAr(ActionEvent event) {
        try {
            SinalAr sinal = new SinalAr();
            sinal.tocar();
        } catch (NoSuchPortException ex) {
            ex.printStackTrace();
            errorDialog(ex.getMessage(), "ERRO");
        } catch (PortInUseException ex) {
            ex.printStackTrace();
            errorDialog(ex.getMessage(), "ERRO");
        } catch (UnsupportedCommOperationException ex) {
            errorDialog(ex.getMessage(), "ERRO");
        }   
    }
    
    @FXML
    private void clickBotaoGeral(ActionEvent event) {
        try {
            SinalGeral sinal = new SinalGeral();
            sinal.tocar();
        } catch (NoSuchPortException ex) {
            ex.printStackTrace();
            errorDialog(ex.getMessage(), "ERRO");
        } catch (PortInUseException ex) {
            ex.printStackTrace();
            errorDialog(ex.getMessage(), "ERRO");
        } catch (UnsupportedCommOperationException ex) {
            errorDialog(ex.getMessage(), "ERRO");
        }  
    }
    
    @FXML
    private void clickBotaoNovo(ActionEvent event) throws IOException, SQLException {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddHorario.fxml"));     
            Parent root = (Parent)fxmlLoader.load();          
            AddHorarioController controller = fxmlLoader.<AddHorarioController>getController();
            controller.setCampos(new Horario());
            Scene scene = new Scene(root); 
            stage.setScene(scene);    
            stage.getIcons().add(new Image("icones/speaker.png"));
            stage.setTitle("Adicionar Horario"); 
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();  
        
        Agendamento ag = Agendamento.getAgendamento(this);
        ag.setAgendamento(null, this);
        List<Horario> horarios = ag.getHorarios();
        preencheTabela(horarios);
    }

    @FXML
    private void clickBotaoEditar(ActionEvent event) throws IOException, SQLException {
        Horario horario = tvHorario.getSelectionModel().getSelectedItem();
        
        if(horario == null){
            warningDialog("Nenhum registro selecionado", "Aviso");
        }else{
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddHorario.fxml"));     
            Parent root = (Parent)fxmlLoader.load();          
            AddHorarioController controller = fxmlLoader.<AddHorarioController>getController();
            controller.setCampos(horario);
            Scene scene = new Scene(root); 
            stage.setScene(scene);    
            stage.getIcons().add(new Image("icones/speaker.png"));
            stage.setTitle("Adicionar Horario"); 
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();  
        }
        
        Agendamento ag = Agendamento.getAgendamento(this);
        ag.setAgendamento(null, this);
        List<Horario> horarios = ag.getHorarios();
        preencheTabela(horarios);
    }

    @FXML
    private void clickBotaoDeletar(ActionEvent event) {
        Horario horario = tvHorario.getSelectionModel().getSelectedItem();
        SQLiteJDBC db = new SQLiteJDBC();
        PreparedStatement pstmt;
        List<Horario> horarios;        
        
        if(horario == null){
            warningDialog("Nenhum registro selecionado", "Aviso");
        }else{
            try (Connection con = db.SQLiteConnect()) {
                pstmt = con.prepareStatement("DELETE FROM Horario WHERE ID = ?");
                pstmt.setInt(1, horario.getID());
                pstmt.execute();
                pstmt.close();
                con.close();
                
                confirmDialog("Registro Excluido com Sucesso.", "Sucesso");    
                Agendamento ag = Agendamento.getAgendamento(this);
                ag.setAgendamento(null, this);
                horarios = ag.getHorarios();
                preencheTabela(horarios);
            }catch(SQLException ex){
                errorDialog("Erro ao deletar o registro do banco de dados", "Erro");
            }
        }       
    }
    
    private void confirmDialog(String msg, String title){
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(msg);
        alert.showAndWait();
    }
    
    private void warningDialog(String msg, String title){
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(msg);
        alert.showAndWait();
    }
    
    private void errorDialog(String msg, String title){
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(msg);
    }
    
    @FXML
    private void eventoDoubleClick(javafx.scene.input.MouseEvent event) {
        if(event.getClickCount() == 2)
        {
            if (tvHorario.getSelectionModel().getSelectedIndex()>=0)
            {
                errorDialog("FUNFOU", "Double Click");
            }
        }
    }
    
    public void setStatus(String msg){
        this.labelStatus.setText(msg);
    }            
    
    public void setStatusColor(int cor){
        String color;
        
        switch(cor){
            case 0:
                color = "#00ff00"; //Verde
                break;
            default:
                color = "#ff0000"; //Vermelho
                break;
        }
        
        this.labelStatus.setTextFill(Color.web(color));
    }
    
    public void preencheTabela(List<Horario> lista){
        tvHorario.getColumns().clear();
        tvHorario.getItems().clear();
        
        TableColumn colNome, colHorario, colDias;
        colNome = new TableColumn<>("Nome do Alarme");
        colHorario = new TableColumn<>("Horário");
        colDias = new TableColumn<>("Dias");
        
        colNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        colNome.setPrefWidth(208);
        colHorario.setCellValueFactory(new PropertyValueFactory<>("hora"));
        colHorario.setPrefWidth(200);
        colDias.setCellValueFactory(new PropertyValueFactory<>("dias"));
        colDias.setPrefWidth(180);
        
        tvHorario.getColumns().addAll(colNome, colHorario, colDias);
        
        ObservableList<Horario> modelo = FXCollections.observableArrayList();      
        
        for(Horario aux : lista){
            String dias = "";
            bean_horario bh = new bean_horario();
            if(aux.isSeg())
                dias = dias+"Seg";
            if(aux.isTer()){
                if(dias.equals(""))
                    dias = dias+"Ter";
                else
                    dias = dias+", Ter";
            }
            if(aux.isQua()){
                if(dias.equals(""))
                    dias = dias+"Qua";
                else
                    dias = dias+", Qua";
            }
            if(aux.isQui()){
                if(dias.equals(""))
                    dias = dias+"Qui";
                else
                    dias = dias+", Qui";
            }
            if(aux.isSex()){
                if(dias.equals(""))
                    dias = dias+"Sex";
                else
                    dias = dias+", Sex";
            }
            
            aux.setDias(dias);
            modelo.add(aux);
        }
                 
        tvHorario.setItems(modelo);
    }

    public void shutdown() throws SQLException, SchedulerException{
        Agendamento ag = Agendamento.getAgendamento(this);
        ag.stopSchedule();
        System.out.println("Stop");
    }
}
