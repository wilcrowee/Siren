package Model;

import gnu.io.*;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class Sinal implements Job{
    protected SerialPort serialPort;
    protected long TEMPO = 3000;

    public Sinal() throws NoSuchPortException, PortInUseException, UnsupportedCommOperationException {
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier("COM1");
        CommPort commPort = portIdentifier.open(this.getClass().getName(), 2000);
        if (commPort instanceof SerialPort) {
            serialPort = (SerialPort) commPort;
            serialPort.setSerialPortParams(57600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
        }
    }

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        tocar();
    }

    public void tocar() {
        serialPort.setDTR(true);
        serialPort.setRTS(true);
        serialPort.sendBreak(1);
        
        try {
            Thread.sleep(TEMPO);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        
        serialPort.setDTR(false);
        serialPort.setRTS(false);
        serialPort.close();
    }
}
