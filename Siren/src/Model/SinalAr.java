package Model;

import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.UnsupportedCommOperationException;

public class SinalAr extends Sinal{

    public SinalAr() throws NoSuchPortException, PortInUseException, UnsupportedCommOperationException {
        super();
        TEMPO = 2000;
    }

    @Override
    public void tocar() {
        for (int i = 0; i < 3; i++) {
            serialPort.setDTR(true);
            serialPort.setRTS(true);
            serialPort.sendBreak(1);
            
            try {
                Thread.sleep(TEMPO);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            
            serialPort.setDTR(false);
            serialPort.setRTS(false);
            
            try {
                Thread.sleep(TEMPO - 700);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        
        serialPort.close();
    }
}
