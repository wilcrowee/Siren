package Model;

import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.UnsupportedCommOperationException;

public class SinalAviso extends Sinal{
    public SinalAviso() throws NoSuchPortException, PortInUseException, UnsupportedCommOperationException {
        super();
        TEMPO = 1800;
    }
}
