package Model;

import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.UnsupportedCommOperationException;


public class SinalGeral extends Sinal{
    public SinalGeral() throws NoSuchPortException, PortInUseException, UnsupportedCommOperationException {
        super();
        TEMPO = 500;
    }
    
    @Override
    public void tocar() {
        for (int i = 0; i < 10; i++) {
            serialPort.setDTR(true);
            serialPort.setRTS(true);
            serialPort.sendBreak(1);
            
            try {
                Thread.sleep(TEMPO);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            
            serialPort.setDTR(false);
            serialPort.setRTS(false);
            
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        
        serialPort.close();
    }
}

